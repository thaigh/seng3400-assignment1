SENG3400 - Assignment 1
=======================

### Authors ###

Tyler Haigh - C3182929

# Abstract #

This project is developed as an assignment for SENG3400 - Network and Distributed Systems at the University of Newcastle, 2015.

This project aims to develop a simple client-server application for sending messages using Java Sockets over a network using a defined protocol

# Java Version #

This project has been built using 

```
Java version "1.8.0_45"
Java(TM) SE Runtime Environment (build 1.8.0_45-b14)
Java HotSpot(TM) 64-Bit Server VM (build 25.45-b02, mixed mode)
```

but has been verified to work on the University machines using

```
java version "1.6.0_35"
OpenJDK Runtime Environment (IcedTea6 1.13.7) (rhel-1.13.7.1.el6_6-x86_64)
OpenJDK 64-Bit Server VM (build 23.25-b01, mixed mode)
```

# To compile this project #

Run the following commands from the `src` folder to compile
```
javac client/*.java
javac server/*.java
```

# To run this project #

This project has been divided into two java packages (client and server). To run the server component:

* Run `java server.CodeConverter` from the root directory. Note that running  `java CodeConverter` from the `server` directory will not execute the program

To run the Client component:

* Run `java client.CodeClient` from the root directory. Note that running  `java CodeClient` from the `client` directory will not execute the program

## Arguments ##

The CodeConverter application can have an optional override for the default port number (25565) by providing the port number as a command line argument e.g. `java server.CodeClient 12345` will override the default port number and run the server on port 12345

The CodeClient application has two options for overriding defaults:

* To override the server address (defaults to the computer's IP address or localhost), run `java client.CodeClient 123.456.1.10` to connect to the server running on IP address 123.456.1.10
* To override the server port number (defaults to 25565) requires the server address to be provided as well. Running `java client.CodeClient 123.456.1.10 12345` will start the client and connect to the server at IP address 123.456.1.10 on port number 12345