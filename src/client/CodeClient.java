package client;

import server.Protocol;
import server.ServerConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * The client component for the application  responsible for establishing
 * a connection to the server hosted application
 */
public class CodeClient {

    // Server Details
    private String serverAddress = ServerConfig.getServerAddress();
    private int serverPort = ServerConfig.getServerPortNumber();

    // Resources. Because the university machines do not have Java 7 for Try-With-Resources
    private Socket clientSocket;
    private PrintWriter clientOut;
    private BufferedReader clientIn;
    private BufferedReader stdIn;

    /**
     * The entry point to the client application
     *
     * Arguments:
     *   - ServerAddress
     *   - ServerAddress PortNumber
     * @param args ServerAddress or (ServerAddress and PortNumber)
     */
    public static void main(String[] args) {

        // Get server address and server port from user
        // Get arguments from command line
        String address = (args.length > 0) ? args[0] : ServerConfig.getServerAddress();

        int port = ServerConfig.getServerPortNumber();
        if (args.length == 2) {
            try { port = Integer.parseInt(args[1]); }
            catch (NumberFormatException e) {
                System.err.println("Error: Port number is invalid");
                System.exit(1);
            }
        }

        if (args.length > 2) {
            System.err.println("Error: Too many arguments provided");
            System.exit(1);
        }

        // Check Port number
        if (port < 1024) {
            System.err.println("Error: Invalid Port Number. Ports 0-1023 are system-reserved");
            System.exit(1);
        }

        CodeClient client = new CodeClient(address, port);
        client.run();
    }

    /**
     * Creates an instance of the client application to connect to the server
     * application at the given address and port number
     *
     * @param address The IP address of the server machine hosting the CodeConverter application
     * @param port The port number of the server application to connect to
     */
    public CodeClient(String address, int port) {
        // Initialisation logic here
        this.serverAddress = address;
        this.serverPort = port;
    }

    /**
     * Prints a client oriented message with a new line terminator
     * @param message The message to display
     */
    public void printClientMessage(String message) {
        printClientMessage(message, true);
    }

    /**
     * Prints a client oriented message
     * @param message The message to display
     * @param newLine Whether to print a new line terminator or not
     */
    public void printClientMessage(String message, boolean newLine) {
        if (newLine)
            System.out.println("CLIENT: " + message);
        else
            System.out.print("CLIENT: " + message);
    }

    /**
     * Prints a server oriented message with a new line terminator
     * @param message The message to display
     */
    public void printServerMessage(String message) {
        printServerMessage(message, true);
    }

    /**
     * Prints a server oriented message
     * @param message The message to display
     * @param newLine Whether to print a new line terminator or not
     */
    public void printServerMessage(String message, boolean newLine) {
        if (newLine)
            System.out.println("SERVER: " + message);
        else
            System.out.print("SERVER: " + message);
    }

    /**
     * Attempts to retrieve a message from the server message buffer
     * @param reader The message stream to read from
     * @return A message if it exists
     */
    public String getServerMessage(BufferedReader reader) {
        String message = null;

        try { message = reader.readLine(); }
        catch (IOException io) { /* Do nothing */ }

        return message;
    }

    /**
     * Main method of the client application which communicates with the
     * Server application
     */
    public void run() {

        printClientMessage("Connecting to " + serverAddress + ":" + serverPort + "...");

        // Setup sockets and buffers using try-with-resources
        try {
            // Initialise resources. Because uni machines can't use Try-With-Resources
            clientSocket = new Socket(serverAddress, serverPort);
            clientOut = new PrintWriter(clientSocket.getOutputStream(), true);
            clientIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            stdIn = new BufferedReader(new InputStreamReader(System.in));

            // As soon as the client establishes a connection, send the ASCII command
            // Client sends ASCII to server to initiate communication
            printClientMessage(Protocol.AsciiMessage);
            clientOut.println(Protocol.AsciiMessage);
            clientSocket.setSoTimeout(ServerConfig.getSocketTimeout());

            String fromServer, fromUser = "";
            while ( (fromServer = getServerMessage(clientIn)) != null ) {
                // Receive message from server
                printServerMessage(fromServer);

                // Check for BYE or END message from server
                if (fromServer.equals(Protocol.ByeResponse) ||
                    fromServer.equals(Protocol.EndResponse))
                    break;

                // Check again
                String secondMessage = getServerMessage(clientIn);
                if (secondMessage != null) {
                    printServerMessage(secondMessage);
                    fromServer = secondMessage;
                }

                // Send message to server
                printClientMessage("Enter command to send to server [AC,CA,BYE,END, or something to convert]: ", false);
                fromUser = stdIn.readLine();
                if (fromUser != null) {
                    printClientMessage(fromUser);
                    clientOut.println(fromUser);
                }
            }

            // If we are here, then we have disconnected from the server for some reason
            // Display whether we disconnected, or something happened server side
            if (fromUser != null && !fromUser.equals(Protocol.ByeMessage) && !fromUser.equals(Protocol.EndMessage))
                System.err.println("Error: Server has been disconnected");
            else
                printClientMessage("Successfully disconnected from server");

        } catch (UnknownHostException h) {
            System.err.println("Error: The host could not be determined. Details: " + h.getMessage());
            System.exit(1);
        } catch (ConnectException con) {
            System.err.println("Error: Unable to connect to server at " + serverAddress + ":" + serverPort
                    + ". Details: " + con.getMessage());
            System.exit(1);
        } catch (IOException io) {
            System.err.println("Error: An IO Error occurred. Details: " + io.getMessage());
            System.exit(1);
        } finally {
            dispose();
        }

    }

    /**
     * Closes all resources used by the client. This is required because Java 6 does not support Try-With-Resources
     * which simplifies the task of managing resources
     */
    private void dispose() {
        try { if (stdIn != null) stdIn.close(); stdIn = null; }
        catch (IOException io) {System.err.println("Error: Unable to close user input stream. Details: " + io.getMessage()); }

        try { if (clientIn != null) clientIn.close(); clientIn = null; }
        catch (IOException io) { System.err.println("Error: Unable to close socket input stream. Details: " + io.getMessage()); }

        clientOut.close();
        clientOut = null;

        try { if (clientSocket != null) clientSocket.close(); clientSocket = null; }
        catch (IOException io) { System.err.println("Error: Unable to close socket. Details: " + io.getMessage()); }

    }
}
