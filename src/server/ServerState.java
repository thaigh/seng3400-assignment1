package server;

/**
 * Defines the various states of the CodeClient server
 */
public enum ServerState {

    /**Initialised State. Conversion method not defined*/
    Initalised,

    /**ASCII message received, ready to take inputs*/
    Ready,

    /** ASCII Value to Character*/
    AC,

    /** Character to ASCII value*/
    CA,

}
