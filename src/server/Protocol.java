package server;

/**
 * Defines the protocol for the application and conversion of
 * input to output messages
 */
public class Protocol {

    public static final String AsciiMessage = "ASCII";
    public static final String AcMessage = "AC";
    public static final String CaMessage = "CA";
    public static final String ByeMessage = "BYE";
    public static final String EndMessage = "END";

    public static final String EndResponse = "END: OK";
    public static final String ByeResponse = "BYE: OK";
    public static final String AsciiOK = "ASCII: OK";
    public static final String ChangeOK = "CHANGE: OK";
    public static final String ErrorResponse = "ERR";

    private ServerState state;

    /**
     * Initialise a protocol to await the ASCII message
     */
    public Protocol() {
        // Initialisation logic
        state = ServerState.Initalised;
    }

    /**
     * Attempts to convert an input message to its equivalent output
     * based on the current state of the Server
     * @param input The input message
     * @return The converted message
     */
    public String processInput(String input) {

        // Check for terminator messages
        if (input.equals(ByeMessage)) return ByeResponse;
        else if (input.equals(EndMessage)) return EndResponse;

        // Check if state is changing and update
        else if (input.equals(AsciiMessage) && state == ServerState.Initalised) {
            updateState(ServerState.CA); return AsciiOK;
        } else if (input.equals(AcMessage)) {
            updateState(ServerState.AC); return ChangeOK;
        } else if (input.equals(CaMessage)) {
            updateState(ServerState.CA); return ChangeOK;
        }

        else {
            switch (state) {
                case Initalised: case Ready: {
                    // Server is not in proper state yet
                    break;
                }
                case AC: {
                    if (validInput(input, state)) {
                        int i = Integer.parseInt(input);
                        char c = AC(i);
                        return String.valueOf(c);
                    }
                }
                case CA: {
                    if (validInput(input, state)) {
                        char c = input.charAt(0);
                        int a = CA(c);
                        return String.valueOf(a);
                    }
                }
                default: break;
            }
        }

        return ErrorResponse;
    }

    /**
     * Updates the server's internal state to the given value.
     * Whilst this method seems redundant, it is included in the interest
     * of scalability if required.
     *
     * @param newState The new state to update to
     */
    public void updateState(ServerState newState) { this.state = newState; }

    /**
     * Converts a single character input to its corresponding ASCII value
     *
     * @param c The character input
     * @return The ASCII integer value
     */
    public int CA(char c) { return (int)c; }

    /**
     * Converts an ASCII number to the corresponding character value
     *
     * @param a The ASCII integer value
     * @return The corresponding character
     */
    public char AC(int a) { return (char)a; }

    /**
     * The converter only works for the ten digits, twenty-siz lower case alphabet characters
     * and the twenty-six upper case alphabet characters. If the client provides a character or
     * number input outside of these ranges, the server returns ERR.
     *
     * The ascii code value must be a string representing a nun-negative integer of appropriate value
     * and the character value must be a single character string of appropriate value
     *
     * @param input
     * @param state
     * @return
     */
    public boolean validInput(String input, ServerState state) {
        int value = -1;

        switch (state) {
            case AC: {
                try {
                    value = Integer.parseInt(input);
                    break;
                } catch (NumberFormatException n) { return false; }
                catch (Exception e) { return false; }
            }
            case CA: {
                if (input.length() > 1) return false;
                else if (input.length() > 0){
                    // Requires secondary check. Somehow managed to send an empty string
                    char c = input.charAt(0);
                    value = (int)c;
                    break;
                } else { return false; }
            }
        }

        // We have a valid format for the input, now check
        // it is in the appropriate range
        return ((value >= 48 && value <= 57) ||
                (value >= 65 && value <= 90) ||
                (value >= 97 && value <= 122) );

    }
}
