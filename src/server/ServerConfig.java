package server;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Defines the default connections for the application
 */
public class ServerConfig {

    private static InetAddress inet;

    /**
     * Sets the InetAddress to allow calculation of the machine name
     * and address
     */
    private static void setINet() {
        if (inet == null) {
            try {
                inet = InetAddress.getLocalHost();
            } catch (UnknownHostException e) {
                //e.printStackTrace();
            }
        }
    }

    /**
     * Gets the host name of the machine
     * @return The host name or 'localhost' if unable to calculate
     */
    public static String getServerName() {
        setINet();
        return (inet == null) ? "localhost" : inet.getHostName();
    }

    /**
     * Gets the IP address of the machine
     * @return The IP address or 'localhost' if unable to calculate
     */
    public static String getServerAddress() {
        setINet();
        return (inet == null) ? "localhost" : inet.getHostAddress();

    }

    /**
     * Gets the default server port number for the application
     * @return 25565 (The default port number)
     */
    public static int getServerPortNumber() { return 25565; }

    /**
     * Gets the default socket timeout limit
     * @return 1000 (The default socket timeout limit)
     */
    public static int getSocketTimeout() { return 1000; }

}
