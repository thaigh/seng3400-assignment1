package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * The server component of the application which converts an input character to its ASCII
 * value or an ASCII value to its character equivalent
 */
public class CodeConverter {

    private int portNumber = ServerConfig.getServerPortNumber();
    private String lastMessage;

    // Resources
    ServerSocket serverSocket;
    Socket clientSocket;
    PrintWriter serverOut;
    BufferedReader serverIn;

    /**
     * The entry point to the application which starts the server on the port
     * specified by the user or the default port as specified by the ServerConfig
     *
     * Arguments:
     * - PortNumber
     *
     * @param args The port number to override the default
     */
    public static void main(String[] args) {

        // Get server port from user
        // Get arguments from command line
        int port = ServerConfig.getServerPortNumber();
        if (args.length == 1) {
            try { port = Integer.parseInt(args[0]); }
            catch (NumberFormatException e) {
                System.err.println("Error: Port number is invalid");
                System.exit(1);
            }
        }

        if (args.length > 1) {
            System.err.println("Error: Too many arguments provided");
            System.exit(1);
        }

        // Check Port number
        if (port < 1024) {
            System.err.println("Error: Invalid Port Number. Ports 0-1023 are system-reserved");
            System.exit(1);
        }

        CodeConverter server = new CodeConverter(port);
        server.run();
    }

    /**
     * Starts the CodeConverter server on the specified port
     * @param port The port number to allow communication through
     */
    public CodeConverter(int port) {
        // Initialisation logic
        this.portNumber = port;
        lastMessage = "";
    }

    /**
     * Prints a server oriented message with a new line terminator
     * @param message The message to display
     */
    public void printServerMessage(String message) {
        printServerMessage(message, true);
    }

    /**
     * Prints a server orientated message
     * @param message The message to display
     * @param newLine Whether to print a new line terminator or not
     */
    public void printServerMessage(String message, boolean newLine) {
        if (newLine)
            System.out.println("SERVER: " + message);
        else
            System.out.print("SERVER: " + message);
    }

    /**
     * Main method of the code converter server which keeps the server running
     * until it receives the END command from a client. It permits up to one
     * connection to a client at a time
     */
    public void run() {

        printServerMessage("Running server at " + ServerConfig.getServerAddress() + ":" + portNumber);

        // Loop while server is running
        while (!lastMessage.equals(Protocol.EndMessage)) {

            try {
                // Setup resources for sockets and input/output buffers
                serverSocket = new ServerSocket(portNumber);
                clientSocket = serverSocket.accept();
                serverOut = new PrintWriter(clientSocket.getOutputStream(), true);
                serverIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                String inputLine, outputLine;

                // Initiate conversation with client using protocol
                // Send OK message to client to indicate ready to receive input
                Protocol protocol = new Protocol();
                lastMessage = "";
                printServerMessage("Connected to client!");

                // Get input from client
                while ((inputLine = serverIn.readLine()) != null) {
                    printServerMessage("Received message: " + inputLine);
                    lastMessage = inputLine;

                    // Use protocol to calculate output
                    outputLine = protocol.processInput(inputLine);
                    printServerMessage("Sending message: " + outputLine);

                    // Print output to client
                    // Signal ASCII OK to client if required
                    if (!outputLine.equals(Protocol.ByeResponse) && !outputLine.equals(Protocol.AsciiOK) && !outputLine.equals(Protocol.EndResponse))
                        outputLine += "\n" + Protocol.AsciiOK;

                    serverOut.println(outputLine);

                    if (outputLine.equals(Protocol.EndResponse)) break;

                }
            } catch (SocketException sex) {
                System.err.println("Error: A socket error occurred. Details: " + sex.getMessage());
            } catch (IOException io) {
                System.err.println("Error: An IO error occurred. Details: " + io.getMessage());
            } finally {
                // Close sockets / buffers
                dispose();
            }

            printServerMessage("Disconnected from Client!");
        }

        // Shutdown Server
        printServerMessage("Stopping Server!");

    }

    /**
     * Closes all resources used by the server. This is required because Java 6 does not support Try-With-Resources
     * which simplifies the task of managing resources
     */
    private void dispose() {
        try { if (serverIn != null) { serverIn.close(); serverIn = null; } }
        catch (IOException io) { System.err.println("Error: Unable to close socket input stream. Details: " + io.getMessage()); }

        serverOut.close();
        serverOut = null;

        try { if (clientSocket != null) { clientSocket.close(); clientSocket = null; } }
        catch (IOException io) { System.err.println("Error: Unable to close client socket. Details: " + io.getMessage()); }

        try { if (serverSocket != null) { serverSocket.close(); serverSocket = null; } }
        catch (IOException io) { System.err.println("Error: Unable to close server socket. Details: " + io.getMessage()); }

    }

}
